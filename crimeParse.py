import ijson
import json

import time

import sys

from buzhug import Base

majorCrimesFile = open("crimeTypes")
majorCrimeTypes =[line.rstrip('\n') for line in majorCrimesFile]

print majorCrimeTypes

def createDB():
    global database
    try:
        database = Base("crimeDB")
        database.create(("date",unicode),("primary_type",unicode),("latitude",unicode),("longitude",unicode),("description",unicode),\
                        ("community_area",unicode),("year",int),("season",int),("weekday",int),("hour",int))
    except:
        database = Base("crimeDB").open()
    print database


def removeUnusedFields(object):
    wantedKeys = ["date","primary_type","latitude","longitude","description","community_area"]
    try:
        newJson = dict([(i,object[i]) for i in wantedKeys])
        parsedTime = time.strptime(str(newJson["date"]),"%Y-%m-%dT%H:%M:%S")
        newJson["year"] = parsedTime.tm_year
        newJson["season"] = get_season(parsedTime)
        newJson["weekday"] = parsedTime.tm_wday
        newJson["hour"] = parsedTime.tm_hour
        database.insert(newJson["date"],newJson["primary_type"],newJson["latitude"],newJson["longitude"],newJson["description"],\
                        newJson["community_area"],newJson["year"],newJson["season"],newJson["weekday"],newJson["hour"])
    except:
        print "Exception"
        return None
    return newJson

                        
def writeValue(file,object):
    if(object["primary_type"] in majorCrimeTypes):
        try:
            file.write(json.dumps(removeUnusedFields(object)))
            file.write(",")
            return True
        except:
            print "Found one with missing data"
            return False

def get_season(fromDate):
    m = float(fromDate.tm_mon) * 100
    d = float(fromDate.tm_mday)
    d = 1
    md = m + d
    
    if ((md > 320) and (md < 621)):
        s = 0 #spring
    elif ((md > 620) and (md < 923)):
        s = 1 #summer
    elif ((md > 922) and (md < 1223)):
        s = 2 #fall
    else:
        s = 3 #winter
    return s



def parseAndMakeDB():
    
    createDB()
    
    objects = ijson.items(open("crimedata"),"item")

    currentYear = 2013

    totalRead = None

    objects = ijson.items(open("crimedata"),"item")
    totalRead = 0
    f = open(("crime"+str(currentYear)),"w")
    f.write("[")
    for object in objects:
        if(object["year"] == str(currentYear)):
            if(writeValue(f,object)):
                pass			
        else:
            f.seek(-1,1) #Go to end of file and replace comma with ]
            f.write("]")
            f.close()
            print "On Year:" + str(currentYear)
            currentYear-=1
            if(object["year"] == str(currentYear)):
                f = open(("crime"+str(currentYear)),"w")
                f.write("[")
                writeValue(f,object)

        totalRead+=1
        print totalRead

    f.close()
    crimesDB.close()

    print "Exiting"

parseAndMakeDB()
