
from urllib2 import urlopen
from json import load
import json

import urllib

def downloadCrimes(total=None):
    #http://data.cityofchicago.org/resource/ijzp-q8t2.json
    hostname = "data.cityofchicago.org/resource/ijzp-q8t2.json"

    f = open('crimedata','wb')
    
    f.write("[")
    pageSize = 1000
    pageNum = 0
    totalReceived = 0
    while True and (total is None or totalReceived < total):
        url = urllib.quote("http://%s?$limit=%s&$offset=%s&$order=date DESC" % (hostname, pageSize,pageNum*pageSize),safe="%/:=&?~#+!$,;'@()*[]")
        response = urlopen(url)
        views = load(response)
        numViewsRcvd = len(views)
        totalReceived += numViewsRcvd
        if numViewsRcvd == 0:
            break
        print '\n======  PAGE %s - %s VIEWS DOWNLOADED:  ======' % (pageNum, numViewsRcvd)
        for view in views:
            f.write(json.dumps(view))
            f.write(",")
        pageNum += 1
            

    f.seek(-1,1)
    f.write("]")
    f.close()
    

def parseFile():
    f = open('crime2011','r').read()
    test = json.loads(f)

#downloadCrimes(2)
parseFile()
print "Done parsing, valid JSON"