import threading

from urllib2 import urlopen
from json import load
import ijson

import urllib

from math import *
from euclid import *
from omega import *
from cyclops import *

import time

import random


# makes use of python utm converter from https://pypi.python.org/pypi/utm
import utm

class CrimeDataLoader (threading.Thread):
    def __init__(self,scene,filename,total=None):
        threading.Thread.__init__(self)
        self.total = total
        self.scene = scene
        self.filename = filename

    
    def run(self):
        self.loadCrimesFromFile(self.filename)


    def loadCrimesFromWeb(self,total):
        #http://data.cityofchicago.org/resource/ijzp-q8t2.json
        hostname = "data.cityofchicago.org/resource/ijzp-q8t2.json"

        pageSize = 20
        pageNum = 0
        while (pageSize*pageNum < total):
            url = urllib.quote("http://%s?$offset=%s&$limit=%s&$order=date DESC" % (hostname, pageNum*pageSize, pageSize),safe="%/:=&?~#+!$,;'@()*[]")
            print url
            response = urlopen(url)
            views = load(response)
            numViewsRcvd = len(views)
            if numViewsRcvd == 0:
                break
            print '\n======  PAGE %s - %s VIEWS DOWNLOADED:  ======' % (pageNum, numViewsRcvd)
            for view in views:
                try:
                    result = utm.from_latlon(float(view['latitude']), float(view['longitude']))
                    model = BoxShape.create(1,1,1)
                    model.setScale(Vector3(20, 20, 20))
                    model.setPosition(Vector3(float(result[0]), float(result[1]), 0))
                    model.setEffect('colored -d green')
                    self.scene.addChild(model)
                except KeyError:
                    print "This one did not have a location"
                pageNum += 1
        return

    def loadCrimesFromFile(self,filename):
        total = 0
#        while(True):
#            print total
#            model = BoxShape.create(1,1,1)
##            self.scene.addChild(model)
#            total += 1
#            time.sleep(0.01)
        objects = ijson.items(open(filename),"item")
        total = 0
        for object in objects:
            try:
                result = utm.from_latlon(float(object['latitude']), float(object['longitude']))
                model = BoxShape.create(1,1,1)
                model.setScale(Vector3(20, 20, 20))
                model.setPosition(Vector3(float(result[0]), float(result[1]), 0))
                model.setEffect('colored -d green')
                self.scene.addChild(model)
                total += 1
                if(self.total is not None and total > self.total):
                    break;
                print total
            except:
                    print object
                    