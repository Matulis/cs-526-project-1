import threading

from urllib2 import urlopen
from json import load
import ijson

from buzhug import Base

import urllib

from math import *
from euclid import *
from omega import *
from cyclops import *

import sprite

import time

import random

import Queue


# makes use of python utm converter from https://pypi.python.org/pypi/utm
import utm

size = sprite.createSizeUniform()
window_size = sprite.createWindowSizeUniform()

iconDict = {"KIDNAPPING":"icons/kidnapping.png","ROBBERY":"icons/robbery.png","BURGLARY":"icons/burglary.png","HOMICIDE":"icons/homicide.png","ARSON":"icons/arson.png","CRIM SEXUAL ASSAULT":"icons/sexual.png"}

class CrimeDataLoader (threading.Thread):
    
    importedKeys = set()
    crimesDB = None
    fulldayFilter = range(0,7)
    fulltypeFilter = [unicode("KIDNAPPING"),unicode("ROBBERY"),unicode("BURGLARY"),unicode("HOMICIDE"),unicode("ARSON"),unicode("CRIM SEXUAL ASSAULT")]
    fullhourFilter = range(0,24)
    fullseasonFilter = range(0,4)
    fullyearFilter = range(2001,2014)
    
    filterResults = set()
    
    dayFilter = []
    typeFilter = []
    hourFilter = []
    seasonFilter = []
    yearFilter = []
    
    jobs = Queue.Queue()
    loadJobs = Queue.Queue()
    loadTotalToProcess = 0
    loadTotalProcessed = 0
    totalToProcess = 0
    totalProcessed = 0
    
    def __init__(self,scene,filename,total=None):
        threading.Thread.__init__(self)
        self.total = total
        self.scene = scene
        self.filename = filename

    
    def run(self):
        self.loadCrimesFromFile(self.filename)
    
    def addDayFilter(self,day):
        if day in self.dayFilter:
            self.dayFilter.remove(day)
        else:
            self.dayFilter.append(day)
        self.filterCrimes()
    
    def addTypeFilter(self,type):
        if unicode(type) in self.typeFilter:
            self.typeFilter.remove(unicode(type))
        else:
             self.typeFilter.append(unicode(type))
        self.filterCrimes()
    
    def addHourFilter(self,hour):
        if hour in self.hourFilter:
            self.hourFilter.remove(hour)
        else:
            self.hourFilter.append(hour)
        self.filterCrimes()
    
    def addSeasonFilter(self,season):
        if season in self.seasonFilter:
            self.seasonFilter.remove(season)
        else:
            self.seasonFilter.append(season)
        self.filterCrimes()

    def addYearFilter(self,year):
        if year in self.yearFilter:
            self.yearFilter.remove(year)
        else:
            self.yearFilter.append(year)
        self.filterCrimes()

    def filterCrimes(self):
        self.filterResults = set()
        print time.time()
        self.jobs = Queue.Queue()
        self.scene.setChildrenVisible(False)
        results = self.crimesDB.select(None,"weekday in weekdayList and primary_type in typeList and hour in hourList and season in seasonList and year in yearList",weekdayList = self.dayFilter if len(self.dayFilter) > 0 else self.fulldayFilter,
           typeList=(self.typeFilter if len(self.typeFilter) > 0 else self.fulltypeFilter),
           hourList=(self.hourFilter if len(self.hourFilter) > 0 else self.fullhourFilter),
           seasonList=(self.seasonFilter if len(self.seasonFilter) > 0 else self.fullseasonFilter),
           yearList=(self.yearFilter if len(self.yearFilter)>0 else self.fullyearFilter))
        
        self.totalProcessed = 0
        print time.time()
        for result in results:
            self.filterResults.add(result.__id__)
            if result.__id__ in self.importedKeys:
                self.jobs.put(result.__id__)
#                print time.time()
        print time.time() ,"Done making queue"
        self.totalToProcess = self.jobs.qsize()

#    def filterCrimes(self):
#        self.scene.setChildrenVisible(False)
#        results = eval(searchString)
#        for result in results:
#            if result.__id__ in self.importedKeys:
#                self.scene.getChildByName("crime"+str(result.__id__)).setVisible(True)


    def loadCrimesFromWeb(self,total):
        #http://data.cityofchicago.org/resource/ijzp-q8t2.json
        hostname = "data.cityofchicago.org/resource/ijzp-q8t2.json"

        pageSize = 20
        pageNum = 0
        while (pageSize*pageNum < total):
            url = urllib.quote("http://%s?$offset=%s&$limit=%s&$order=date DESC" % (hostname, pageNum*pageSize, pageSize),safe="%/:=&?~#+!$,;'@()*[]")
            print url
            response = urlopen(url)
            views = load(response)
            numViewsRcvd = len(views)
            if numViewsRcvd == 0:
                break
            print '\n======  PAGE %s - %s VIEWS DOWNLOADED:  ======' % (pageNum, numViewsRcvd)
            for view in views:
                try:
                    result = utm.from_latlon(float(view['latitude']), float(view['longitude']))
                    result = utm.from_latlon(float(object['latitude']), float(object['longitude']))
                    model = sprite.createSprite('{}'.format(iconDict[str(object["primary_type"])]), size, window_size, True)
                    model.setPosition(Vector3(float(result[0]), float(result[1]), 10))
                    #                model.setCullingActive(False)
                    self.scene.addChild(model)
                except KeyError:
                    print "This one did not have a location"
                pageNum += 1
        return

    def loadCrimesFromDB(self,filename,skipByValue=None):
        try:
            self.crimesDB = Base(filename)
            self.crimesDB.open()
        except:
            print "Error opening database. Aborting!!!!"
            return

        total = 0
        objects = self.crimesDB()
    
        counter = 0
        for object in objects:
            counter += 1
            if(skipByValue is not None and counter < skipByValue):
                continue
            
            counter = 0
            self.loadTotalToProcess += 1
            self.filterResults.add(object.__id__)
            self.loadJobs.put(object)
            total += 1
            if(self.total is not None and total > self.total):
                break;

        print "Done loading database"

    def addObjectToScene(self,object):
        try:
            result = utm.from_latlon(float(object.latitude), float(object.longitude))
            model = sprite.createSprite('{}'.format(iconDict[str(object.primary_type)]), size, window_size, True)
            self.importedKeys.add(object.__id__)
            model.setName("crime"+str(object.__id__))
            model.setPosition(Vector3(float(result[0]), float(result[1]), 10))
            #                model.setCullingActive(False)
            if(object.__id__ in self.filterResults):
                model.setVisible(True)
            else:
                model.setVisible(False)
            self.scene.addChild(model)
            total += 1
        except:
            return

    def loadCrimesFromFile(self,filename):
        total = 0
#        while(True):
#            print total
#            model = BoxShape.create(1,1,1)
##            self.scene.addChild(model)
#            total += 1
#            time.sleep(0.01)
        objects = ijson.items(open(filename),"item")
        total = 0
        for object in objects:
            try:
                result = utm.from_latlon(float(object['latitude']), float(object['longitude']))
                model = sprite.createSprite('{}'.format(iconDict[str(object["primary_type"])]), size, window_size, True)
                model.setPosition(Vector3(float(result[0]), float(result[1]), 10))
#                model.setCullingActive(False)
                self.scene.addChild(model)
                total += 1
                if(self.total is not None and total > self.total):
                    break;
                print total
            except:
                    print object
                    