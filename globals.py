from caveutil import *

#File containing globals to avoid cyclic dependency

#set up camera interpolator for neigbhorhood movements
interp = InterpolActor(getDefaultCamera())
interp.setTransitionType(InterpolActor.SMOOTH)
interp.setDuration(3)	# Set interpolation duration to 3 seconds
interp.setOperation(InterpolActor.POSITION | InterpolActor.ORIENT)
# Interpolate both position and orientation
