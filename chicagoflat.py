###############################################################
#
# Sample code for CS 526 Fall 2013
# Copyright 2013 by Andrew Johnson, evl, uic
#
# example code integrating osgearth and omegalib
# and overlaying data onto a flat map of the chicago area
#
###############################################################

from math import *
from euclid import *
from omega import *
from cyclops import *

from caveutil import *

import omega

# makes use of python utm converter from https://pypi.python.org/pypi/utm
import utm

import parsing

from crimeMenu import *
from crimedata import *
from globals import *

import sys

# deal with audio
env = getSoundEnvironment()
s_sound = env.loadSoundFromFile("beep", "/menu_sounds/menu_select.wav")

scene = getSceneManager()

# set background to black
scene.setBackgroundColor(Color(0, 0, 0, 1))

# Create a directional light
light1 = Light.create()
light1.setLightType(LightType.Directional)
light1.setLightDirection(Vector3(-1.0, -1.0, -1.0))
light1.setColor(Color(0.5, 0.5, 0.5, 1.0))
light1.setAmbient(Color(0.2, 0.2, 0.2, 1.0))
light1.setEnabled(True)

# Load a static osgearth 'model'
cityModel1 = ModelInfo()
cityModel1.name = "city1"
cityModel1.path = "chicago_flat.map.earth"
scene.loadModel(cityModel1)

# Create a scene object using the loaded model
city1 = StaticObject.create("city1")
city1.getMaterial().setLit(False)

setNearFarZ(1, 2 * city1.getBoundRadius())

# add another version with a different type of map
cityModel2 = ModelInfo()
cityModel2.name = "city2"
cityModel2.path = "chicago_flat.sat.earth"
# cityModel2.path = "chicago_yahoo.earth"
scene.loadModel(cityModel2)

# Create a scene object using the second loaded model
city2 = StaticObject.create("city2")
city2.getMaterial().setLit(False)

#deal with the camera
cam = getDefaultCamera()
cam.setPosition(city1.getBoundCenter() + Vector3(7768.82, 2281.18, 2034.08))
cam.getController().setSpeed(2000)
cam.pitch(math.radians(45)) #pitch up to start off flying over the city


#set up the scene
all = SceneNode.create("everything")
all.addChild(city1)
all.addChild(city2)

#turn off one of the two maps
city1.setVisible(False)

#add some data in lat lon format onto the UTM based map
# chicago is in UTM Zone 16

crimeNodes = SceneNode.create("crime")
all.addChild(crimeNodes)
# first the set of L stops
crimes = CrimeDataLoader(crimeNodes,"crime2013")

# Load CTA Lines and Trains
cta = parsing.TrainLines()
cta.parseLines("CTARailLines.kml",all)

trainNode = SceneNode.create("trainNode")
all.addChild(trainNode)

cta.initializeTrainInfo(trainNode)

#Load CTA Stops
f = open('CTA_L_Stops')
ctastops = [line.rstrip('\n') for line in f]

for name in ctastops:
    #print name
    foo = name.strip(' ()"')
    bar = foo.partition(', ')
    result = utm.from_latlon(float(bar[0]), float(bar[2]))
    model = SphereShape.create(1,1)
    model.setScale(Vector3(40, 40, 40))
    model.setPosition(Vector3(float(result[0]), float(result[1]), 0))
    model.setEffect('colored -d white -e #808080')
    all.addChild(model)
f.close()


##load neighborhoods and create navigation menu
#global neighborhoods
neighborhoods = parsing.Neighborhoods()
neighborhoods.addNeighborhoods("Kmlcommunityareas.kml",all)

neighborhoods.addNeighborhoodMenu()

#Create options and filter menu
crimeMenu = CrimeMenu()
crimeMenu.filterCrimeType()
crimeMenu.optionsMenu()


#Start Loading Crimes
crimes.loadCrimesFromDB("crimeDB",100)
#handle events from the wand
# left and right buttons shift between the two maps

def handleEvent():
    
    global userScaleFactor
    
    e = getEvent()
    
    if(e.isKeyDown(ord('c'))):
        print "Pressed C"
        
    
    if(e.isButtonDown(EventFlags.ButtonLeft)):
        print("Left button pressed")
        city1.setVisible(False)
        city2.setVisible(True)
        
        #play button sound
        si_sound = SoundInstance(s_sound)
        si_sound.setPosition( e.getPosition() )
        si_sound.setVolume(1.0)
        si_sound.setWidth(20)
        si_sound.play()
    
    if(e.isButtonDown(EventFlags.ButtonRight)):
        print("Right button pressed")
        city2.setVisible(False)
        city1.setVisible(True)
        
        #play button sound
        si_sound = SoundInstance(s_sound)
        si_sound.setPosition( e.getPosition() )
        si_sound.setVolume(1.0)
        si_sound.setWidth(20)
        si_sound.play()



# by default user can use the joystick to spin the world and fly
# but a better flying model should be added in

#UI for Percentage Loaded
ui = UiModule.createAndInitialize()
wf = ui.getWidgetFactory()
uiroot = ui.getUi()

percentFilterLabel = wf.createLabel('percentageFilter', uiroot, '%')
percentLoadedLabel = wf.createLabel('percentageLoaded', uiroot, '%')
displaySize = getDisplayPixelSize()

#percentFilterLabel.setStyleValue('align', 'middle-center')
#percentLoadedLabel.setStyleValue('align', 'middle-center')

#if(isCave()):
#percentFilterLabel.setPosition(Vector2(displaySize[0]/2,displaySize[1]/2))
percentFilterLabel.setFont('fonts/segoeuimod.ttf 50')
#percentLoadedLabel.setPosition(Vector2(displaySize[0]/2,(displaySize[1]/2)+250))
percentLoadedLabel.setFont('fonts/segoeuimod.ttf 50')
#else:
percentFilterLabel.setPosition(Vector2(700,200))
#percentFilterLabel.setFont('fonts/segoeuimod.ttf 250')
percentLoadedLabel.setPosition(Vector2(700,250))
#percentLoadedLabel.setFont('fonts/segoeuimod.ttf 250')

numberPerFrame = 40

print displaySize
print "Display size" + str(displaySize[0]) + " " + str(displaySize[1])


def processLoadingJobs():
    processed = 0
    while(not crimes.loadJobs.empty()):
        crimes.addObjectToScene(crimes.loadJobs.get())
        processed +=1
        if(processed>numberPerFrame):
            crimes.loadTotalProcessed += numberPerFrame
            percentLoadedLabel.setText("Percent Loaded:{}".format(((crimes.loadTotalProcessed*100)/crimes.loadTotalToProcess)))
            break
    
    if(crimes.loadJobs.empty()):
        percentLoadedLabel.setVisible(False)
    else:
        percentLoadedLabel.setVisible(True)


def processFilterJobs():
    processed = 0
    while(not crimes.jobs.empty()):
        crimeNodes.getChildByName("crime"+str(crimes.jobs.get())).setVisible(True)
        processed +=1
        if(processed>numberPerFrame):
            crimes.totalProcessed += numberPerFrame
            percentFilterLabel.setText("Percent Filtered:{}".format(((crimes.totalProcessed*100)/crimes.totalToProcess)))
            break
    
    if(crimes.jobs.empty()):
        percentFilterLabel.setVisible(False)
    else:
        percentFilterLabel.setVisible(True)

trainUpdateFrequency = 20
time = 0
def onUpdate(frame, t, dt):
    global time
    processFilterJobs()

    processLoadingJobs()
    time += dt
    if(time > trainUpdateFrequency):
        cta.updateTrainInfo()
        time = 0

    
setEventFunction(handleEvent)
setUpdateFunction(onUpdate)

