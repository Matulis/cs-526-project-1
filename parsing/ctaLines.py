import urllib2
import xml.etree.ElementTree as ET

import re
from HTMLParser import HTMLParser

from math import *
from euclid import *
from omega import *
from cyclops import *

from omegaToolkit import *

from collections import OrderedDict
import operator
import utm
import time

import bs4

values = []

#colorDict = {"Red":"red","Green":"green","Blue":"blue","Yellow":"yellow",
#"Purple":"purple","Pink":"fuchsia","Brown":"#964B00","Orange":"#FF7F00"}

colorDict = {"Red":"red","Green":"green","Blue":"blue","Yellow":"yellow",
"Purple":"purple","Pink":"fuchsia","Brown":"#964B00","Orange":"#FF7F00"}
infoColorDict =OrderedDict({"red":"red","blue":"blue","brn":"#964B00","g":"green","org":"#FF7F00","p":"purple","pink":"fuchsia","y":"yellow"})
infoIndexDict ={"red":1,"blue":2,"brn":3,"g":4,"org":5,"p":6,"pink":7,"y":8}

class TrainLines() :
    trainArray = dict()
    def parseLines(self,filename,scenegraph):
        tree = ET.parse(filename)
        root = tree.getroot()
        
        lines = root.findall('.//{http://www.opengis.net/kml/2.2}Placemark')

        for line in lines:
            coords = line.find(".//{http://www.opengis.net/kml/2.2}coordinates")
            coord_values = re.findall("[0-9,-.]*",coords.text)
            coord_values = filter(None,coord_values) #filter empty strings
            
            coordinates = list()
            for value in coord_values:
                coordinates.append(map(float,re.split("[,]",value)))
            #Parse colors here too
        
            lineColors = line.find(".//{http://www.opengis.net/kml/2.2}name").text.split()
        
            offset = 0
            thickness = 30

            for color in lineColors:
                if color in colorDict:
                    ctaSegment = LineSet.create()
                    ctaSegment.setEffect('colored -d {}'.format(colorDict[color]))
                    oldValue = coordinates[0]
                    for point in coordinates[1:]:
                        start =  utm.from_latlon(oldValue[1],oldValue[0])
                        end = utm.from_latlon(point[1], point[0])
                        line = ctaSegment.addLine()
                        line.setStart(Vector3(float(start[0])+thickness*offset, float(start[1])+thickness*offset,0))
                        line.setEnd(Vector3(float(end[0])+thickness*offset, float(end[1])+thickness*offset,0))
                        line.setThickness(thickness)
                        oldValue = point
                    

                    scenegraph.addChild(ctaSegment)

    def initializeTrainInfo(self,scenegraphNode):
        for color in infoColorDict.iterkeys():
            tempTrainList = []
            for value in range(0,100):
                model = BoxShape.create(60,200,30)
                model.setPosition(Vector3(-1000,-1000,-1000))
                model.setEffect("colored -d {} -e {}".format(infoColorDict[color],infoColorDict[color]))
                scenegraphNode.addChild(model)
                tempTrainList.append(model)
            self.trainArray[color] = tempTrainList
            print infoColorDict[color]
        print "done making trains, CHOO CHOO"
            
    def updateTrainModels(self,scenegraphNode):
        scenegraphNode.setChildrenVisible(False)
    
        tree = ET.parse('cta_locations.xml')
        
        root = tree.getroot()
        
        routes = root.findall('route')
        
        for route in routes:
            routeName = route.get('name')
            color = infoColorDict[routeName]
            trains = route.findall("train")
            for index,train in enumerate(trains):
                
                lat = train.find("lat").text
                lon = train.find("lon").text
                heading = float(train.find("heading").text)
                coord = utm.from_latlon(float(lat),float(lon))
                model = self.trainArray[routeName][index]
                model.setPosition(Vector3(coord[0],coord[1],30))
                model.setOrientation(Quaternion.new_rotate_axis(math.radians(-heading), Vector3(0, 0, 1)))
                model.setVisible(True)
        print "Updated Trains"
            
    def updateTrainInfo(self):
        #change developer key
    
        if(isMaster()):
            trainLocationsXML = urllib2.urlopen('http://lapi.transitchicago.com/api/1.0/ttpositions.aspx?key=afe8af70b3734c8aa441c108f5ef4f30&rt=red,blue,brn,g,org,p,pink,y')
            
            f = open("cta_locations.xml", "w")
            try:
                f.write(trainLocationsXML.read())
            finally:
                f.close()
            broadcastCommand("cta.updateTrainModels(trainNode)")

    
                




