
import xml.etree.ElementTree as ET

import re
from HTMLParser import HTMLParser

from math import *
from euclid import *
from omega import *
from cyclops import *

from omegaToolkit import *

from caveutil import *

import operator
import utm

import bs4

from globals import *

class NeighborhoodNode():
    def __init__(self,coord,name,number,sceneGraphNode):
        self.coord = coord
        self.name = name
        self.number = number
        self.sceneGraphNode = sceneGraphNode
        
    # name = False
    # number = False
    # neighborhoodName = None
    # def handle_starttag(self, tag, attrs):
        # if tag == "span":
            # if "atr-value" in attrs[0]:
                # self.value = True
    # def handle_data(self, data):
        # if self.value == True:
            # self.neighborhoodName = data
            # self.value = False
            # raise FoundValue

class Neighborhoods( ):
    nodes = list()
    sorted_nodes = list()
    communityOutlineNode = SceneNode.create("communityOutline")
    outlineVisible = False
    
    def addNeighborhoods(self,filename, scenegraph):
        tree = ET.parse(filename)
        root = tree.getroot()

        neighborhoods = root.findall('.//{http://www.opengis.net/kml/2.2}Placemark')

        count = 0


        for zone in neighborhoods:
        
            description = zone.find(".//{http://www.opengis.net/kml/2.2}description")
            
            soup = bs4.BeautifulSoup(description.text)
            neighborhoodName = None
            neighborhoodNumber = None
            spans = soup.find_all("span")
            for span in spans:
                if(span.string == "AREA_NUMBE"):
                    neighborhoodNumber = spans[spans.index(span)+1].string
                if(span.string == "COMMUNITY"):
                    neighborhoodName = spans[spans.index(span)+1].string.title()
            
            print neighborhoodName
            lat = zone.find(".//{http://www.opengis.net/kml/2.2}latitude").text
            long = zone.find(".//{http://www.opengis.net/kml/2.2}longitude").text
            
            nameText = Text3D.create('fonts/segoeuimod.ttf', 200,str(neighborhoodName))
            nameText.setFacingCamera(getDefaultCamera())
            nameText.setColor(Color("white"))
            nc =  utm.from_latlon(float(lat),float(long))
            
            nameText.setPosition(Vector3(float(nc[0]),float(nc[1]),100))
            scenegraph.addChild(nameText)
            
            namesphere = SphereShape.create(1,1)
            namesphere.setScale(Vector3(50, 50, 50))
            namesphere.setPosition(Vector3(float(nc[0]), float(nc[1]), 0))
            namesphere.setEffect('colored -d yellow -e yellow')
            scenegraph.addChild(namesphere)
    
            self.nodes.append(NeighborhoodNode(nc,str(neighborhoodName),int(neighborhoodNumber),namesphere))
        
#            img1 = PlaneShape.create(20, 20)
#            img1.setPosition(Vector3(0,20,10))
#            img1.setEffect("textured -v emissive -d bar_3D.png")
#            img1.getMaterial().setTransparent(True)
#            img1.setFacingCamera(getDefaultCamera())
#            namesphere.addChild(img1)
        
            polygon = zone.find(".//{http://www.opengis.net/kml/2.2}Polygon")
            
            coords = polygon.find(".//{http://www.opengis.net/kml/2.2}coordinates")
            
            values = re.split("[\s]",coords.text)
            
            coordinates = list()
            for value in values:
                coordinates.append(map(float,re.split("[,]",value)))
                
            # rewrite this to have point structure with circular buffer?
            oldValue = coordinates[-1]

            
           
            cityzones = LineSet.create()
            cityzones.setEffect('colored -d silver -e silver') 
            for point in coordinates:
                start =  utm.from_latlon(float(oldValue[1]),float(oldValue[0]))
                end = utm.from_latlon(point[1], point[0])
                if(end != start):
                    line = cityzones.addLine()
                    line.setStart(Vector3(float(start[0]), float(start[1]), 20))
                    line.setEnd(Vector3(float(end[0]), float(end[1]), 20))
                    line.setThickness(20)
                oldValue = point
            self.communityOutlineNode.addChild(cityzones)
        self.communityOutlineNode.setChildrenVisible(False)
        scenegraph.addChild(self.communityOutlineNode)

    def addNeighborhoodMenu(self):
        Neighborhoods.sorted_nodes = sorted(self.nodes, key= lambda NeighborhoodNode : NeighborhoodNode.number)

        mm = MenuManager.createAndInitialize()
        neighborhoodMenu = mm.getMainMenu().addSubMenu("Neighborhoods")

        neighborhoodMenuContainer = neighborhoodMenu.getContainer()
        neighborhoodMenuContainer.setLayout(ContainerLayout.LayoutHorizontal)

        verticalLength = 10
        
        # Create 3 vertical columns, add them to the menu
        columns = list()
        for i in range(0,int(math.ceil(len(Neighborhoods.sorted_nodes)/float(verticalLength)))):
            columns.append(Container.create(ContainerLayout.LayoutVertical, neighborhoodMenuContainer))

        buttons = list()
        for idx, value in enumerate(Neighborhoods.sorted_nodes):
            button = Button.create(columns[int(math.floor(idx/float(verticalLength)))])
            button.setText(value.name)
            button.setUIEventCommand("parsing.Neighborhoods.setCameraToNeighborhood(%s)" % (idx))
            buttons.append(button)
        for index, button in enumerate(buttons):
            if(not index >= len(buttons)-verticalLength):
                button.setHorizontalNextWidget(buttons[index+verticalLength])
            if(not index < verticalLength):
                button.setHorizontalPrevWidget(buttons[index-verticalLength])

                    
    def setCommunityOutline(self):
        self.outlineVisible = not self.outlineVisible
        self.communityOutlineNode.setChildrenVisible(self.outlineVisible)


    @staticmethod
    def setCameraToNeighborhood(nodeIndex):
        cameraOffset = 3000
        camera = getDefaultCamera()
        currPos = camera.getPosition()
        interp.setTargetPosition(Point3(Neighborhoods.sorted_nodes[nodeIndex].coord[0],Neighborhoods.sorted_nodes[nodeIndex].coord[1]-cameraOffset,cameraOffset))
        interp.setTargetOrientation(Quaternion(0.9, 0.4, 0.0, 0.0))
#        camera.focusOn(Neighborhoods.sorted_nodes[nodeIndex].sceneGraphNode)
#        camera.setPitchYawRoll(Vector3(radians(0), 0, 0))
       
        interp.startInterpolation()
        print Neighborhoods.sorted_nodes[nodeIndex].name

#b3c3.setHorizontalPrevWidget(b3c2)
#
#
#


# __all__ = ["addNeighborhoods"]


# addNeighborhoods("neighborhoods.kml",None)