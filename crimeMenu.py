from math import *
from euclid import *
from omega import *
from cyclops import *

from omegaToolkit import *

class CrimeMenu():
    
    def filterCrimeType(self):
        mm = MenuManager.createAndInitialize()
        crimeFilterMenu = mm.getMainMenu().addSubMenu("Filter Crimes")
        typeMenu = crimeFilterMenu.addSubMenu("Type")
        self.makeTypeMenu(typeMenu)
    
        dayMenu = crimeFilterMenu.addSubMenu("Day")
        self.makeDayMenu(dayMenu)
            
        hourMenu = crimeFilterMenu.addSubMenu("Hour")
        self.makeHourMenu(hourMenu)
            
        seasonMenu = crimeFilterMenu.addSubMenu("Season")
        self.makeSeasonMenu(seasonMenu)
    
        yearMenu = crimeFilterMenu.addSubMenu("Year")
        self.makeYearMenu(yearMenu)
            
#        realTimeBtn = Button.create(crimeFilterMenu.getContainer())
#        realTimeBtn.setText("Real-Time Crimes")
#        realTimeBtn.setCheckable(True)
#        realTimeBtn.setChecked(False)
    

    def optionsMenu(self):
        mm = MenuManager.createAndInitialize()
        menu = mm.getMainMenu().addSubMenu("Misc Options")

        optionsMenuContainer = menu.getContainer()

        outlineBtn = Button.create(optionsMenuContainer)
        outlineBtn.setText("Show Zone Outline")
        outlineBtn.setCheckable(True)
        outlineBtn.setChecked(False)
        outlineBtn.setUIEventCommand("neighborhoods.setCommunityOutline()")

    def makeDayMenu(self,dayMenu):
        weekdays = {0:"Monday",1:"Tuesday",2:"Wednesday",3:"Thursday",4:"Friday",
            5:"Saturday",6:"Sunday"}
        for value in weekdays.keys():
            button = Button.create(dayMenu.getContainer())
            button.setText(weekdays[value])
            button.setCheckable(True)
            button.setChecked(False)
            button.setUIEventCommand("crimes.addDayFilter({})".format(value))

    def makeTypeMenu(self, typeMenu):
        types = [unicode("KIDNAPPING"),unicode("ROBBERY"),unicode("BURGLARY"),unicode("HOMICIDE"),unicode("ARSON"),unicode("CRIM SEXUAL ASSAULT")]
        for value in types:
            button = Button.create(typeMenu.getContainer())
            button.setText(str(value).title())
            button.setCheckable(True)
            button.setChecked(False)
            button.setUIEventCommand("crimes.addTypeFilter(\""+ str(value) + "\")")

    def makeHourMenu(self, hourMenu):
        types = range(0,24)
        
        hourMenuContainer = hourMenu.getContainer()
        hourMenuContainer.setLayout(ContainerLayout.LayoutHorizontal)
        # Create 3 vertical columns, add them to the menu
        columns = list()
        verticalLength = 6
        columns = list()
        for i in range(0,int(math.ceil(len(types)/float(verticalLength)))):
            columns.append(Container.create(ContainerLayout.LayoutVertical, hourMenuContainer))
        
        buttons = list()
        for idx,value in enumerate(types):
            button = Button.create(columns[int(math.floor(idx/float(verticalLength)))])
            button.setText(str(value).title())
            button.setCheckable(True)
            button.setChecked(False)
            button.setUIEventCommand("crimes.addHourFilter({})".format(value))
            buttons.append(button)
        
        for index, button in enumerate(buttons):
            if(not index >= len(buttons)-verticalLength):
                button.setHorizontalNextWidget(buttons[index+verticalLength])
            if(not index < verticalLength):
                button.setHorizontalPrevWidget(buttons[index-verticalLength])


    def makeSeasonMenu(self,seasonMenu):
        types = range(0,4)
        for value in types:
            button = Button.create(seasonMenu.getContainer())
            button.setText(str(value).title())
            button.setCheckable(True)
            button.setChecked(False)
            button.setUIEventCommand("crimes.addSeasonFilter({})".format(value))

    def makeYearMenu(self,yearMenu):
        types = range(2001,2014)
        for value in types:
            button = Button.create(yearMenu.getContainer())
            button.setText(str(value).title())
            button.setCheckable(True)
            button.setChecked(False)
            button.setUIEventCommand("crimes.addYearFilter({})".format(value))

        


